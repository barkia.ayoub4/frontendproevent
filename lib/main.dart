import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:get/get.dart';

import 'app/application_style/AppColors.dart';
import 'app/routes/app_pages.dart';

void main() {
  timeDilation = 2;
  runApp(
    GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      theme: ThemeData(

        inputDecorationTheme:const InputDecorationTheme(
          prefixIconColor: AppColors.pink_dark,
            labelStyle: TextStyle(color: Colors.black54)
        ),

      ),
    ),
  );
}
