import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../../../utils/TextField.dart';
import '../../../application_style/AppColors.dart';
import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.pink_light,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Form(
          key: controller.loginFormKey,
          child: Column(
            children: [
              const SizedBox(height: 80),
              Image.asset("assets/asm_shadow.png", width: 150),
              const Text(
                'ASM: All Soft Multimedia',
                style: TextStyle(
                  color: Color(0xFF494949),
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(height: 20),
              const Text(
                'Create Account',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 35,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 8),
              const SizedBox(
                width: 300,
                child: Text(
                  "Create an account so you can explore all the existing events",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                        padding: const EdgeInsets.only(
                            left: 36, bottom: 20, top: 20, right: 8),
                        child: CustomInputField(
                            obsecurText: false,
                            label: 'Name',
                            icon: const Icon(Icons.person),
                            controlller: controller.firstNameController)),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          right: 36, bottom: 20, top: 20, left: 8),
                      child: CustomInputField(
                        obsecurText: false,
                          label: 'LastName',
                          icon: const Icon(Icons.person),
                          controlller: controller.lastNameController),
                    ),
                  ),
                ],
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 36),
                  child: CustomInputField(
                    obsecurText: false,
                    label: 'Email',
                    icon: const Icon(Icons.email_outlined),
                    controlller: controller.userMailController,
                  )),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 36, vertical: 20),
                child: CustomInputField(
                  obsecurText: true,
                  label: 'Password',
                  icon: const Icon(Icons.remove_red_eye_rounded),
                  controlller: controller.passwordController,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 36),
                  child: CustomInputField(
                      obsecurText: true,
                      label: 'Confirm Password',
                      icon: const Icon(Icons.remove_red_eye_rounded),
                      controlller: controller.confirmPasswordController)),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: () {
                  controller.registerWithEmail();
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.pink_dark,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 130, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: const Text(
                  'Sign up',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
              TextButton(
                onPressed: () {},
                child: const Text(
                  "Already have an account",
                  style: TextStyle(
                    color: AppColors.pink_dark,
                    fontSize: 14,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const Text(
                'Or continue with',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/google.png", height: 50, width: 50),
                  const SizedBox(width: 16),
                  Image.asset("assets/facebook.png", height: 50, width: 50),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
