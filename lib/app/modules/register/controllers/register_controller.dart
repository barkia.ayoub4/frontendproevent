import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pfe/app/modules/home/views/home_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../data/login_services.dart';

class RegisterController extends GetxController {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController userMailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  GlobalKey<FormState> get  loginFormKey => _formKey;
  final _formKey = GlobalKey<FormState>();
  final ApiService _apiService = ApiService();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

   String? validateEmail(String value){
     if (!GetUtils.isEmail(value)){
       return "Provide valid email";
     }
     return null ;
   }

  String? validatePassword(String value){
    if (value.length<=6){
      return "Password must be of 6 caracteres";
    }
  }

  bool checkLogin(){
     final isvalid=loginFormKey.currentState!.validate();
     if(!isvalid){
       return false;
     }
     loginFormKey.currentState!.save();
     return true;

  }

  Future<void> registerWithEmail() async {
     if(!checkLogin()){
       return;
     }else{
    if (passwordController.text == confirmPasswordController.text) {
      try {
        Map<String, dynamic> data = {
          'firstName': firstNameController.text,
          'lastName': lastNameController.text,
          'email': userMailController.text.trim(),
          'password': passwordController.text,
        };

        var response = await _apiService.registerUser(data);
        if (response != null && response['access_token'] != null) {
          var token = response['access_token'];
          print("Access Token: $token");
          ///TODO inject token using interceptor
          final SharedPreferences preferences = await _prefs;
          await preferences.setString('token', token);
          clearFields();
          Get.offAll(() => const HomeView());
        } else {
          Get.snackbar(
            "Erreur d'inscription",
            "Vous avez déja un compte.",
            snackPosition: SnackPosition.BOTTOM,
          );
        }
      } catch (e) {
        print(e.toString());
        Get.snackbar(
          "Erreur de connexion",
          "Impossible de se connecter au serveur. Veuillez vérifier votre connexion internet et réessayer.",
          snackPosition: SnackPosition.BOTTOM,
        );
      }
    } else {
      Get.snackbar(
        "Erreur de mot de passe",
        "Les mots de passe ne correspondent pas. Veuillez vérifier et réessayer.",
       margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
        snackPosition: SnackPosition.BOTTOM,
        colorText: Colors.white,
        backgroundColor: Colors.red,
      );
    }
  }}


  void clearFields() {
    firstNameController.clear();
    lastNameController.clear();
    userMailController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
