import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OTPVerificationController extends GetxController {
  var timeRemaining = 60.obs;
  var timerActive = true.obs;
  List<TextEditingController> controllers = List.generate(6, (index) => TextEditingController()).obs;
  List<FocusNode> focusNodes = List.generate(6, (index) => FocusNode()).obs;

  @override
  void onInit() {
    super.onInit();
    startTimer();
  }


  @override
  void onClose() {
    controllers.forEach((controller) => controller.dispose());
    focusNodes.forEach((node) => node.dispose());
    super.onClose();
  }

  void startTimer() {
    const oneSecond = Duration(seconds: 1);
    Timer.periodic(oneSecond, (timer) {
      if (timeRemaining.value == 0) {
        timerActive.value = false;
        timer.cancel();
      } else {
        timeRemaining.value--;
      }
    });
  }

  void resendCode() {
    if (!timerActive.value) {
      timeRemaining.value = 60;
      timerActive.value = true;
      startTimer();
    }
  }


}
