import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pfe/app/modules/home/views/home_view.dart';
import 'package:pfe/app/modules/login/views/OTPVerificationScreen.dart';
import 'package:pfe/app/modules/register/views/register_view.dart';

import '../../../../utils/TextField.dart';
import '../../../application_style/AppColors.dart';
import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {

  LoginController controller=Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /// make this screen responsive
      backgroundColor: AppColors.pink_light,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
            children: [
              const SizedBox(height: 100),
              Hero(
                  tag: 'logo',
                  child: Column(
                    children: [
                      /// TODO replace image
                      Image.asset("assets/asm_shadow.png", width: 150),
                      const SizedBox(height: 10),
                      const Text(
                        'ASM: All Soft Multimedia',
                        style: TextStyle(
                          color: Color(0xFF494949),
                          fontSize: 14,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ],
                  )),
              const SizedBox(height: 40),
              const Text(
                'Login here',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 35,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 8),
              const Text(
                "Welcome back you've been missed!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 40),
               Padding(
                  padding:const EdgeInsets.symmetric(horizontal: 30),
                  child: CustomInputField(label: 'Email', icon:const Icon(Icons.email_outlined),controlller:controller.userMailController)
              ),
              const SizedBox(height: 20),
              Padding(
                  padding:const  EdgeInsets.symmetric(horizontal: 30),
                  child:CustomInputField(label: 'Password', icon:const Icon(Icons.remove_red_eye),controlller:controller.passwordController)
              ),
              const SizedBox(height: 20),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.only(right: 36),
                  child: TextButton(
                    onPressed: () {
                      ///TODO change it with get.to
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => OTPVerificationScreen()),
                      );
                    },
                    child:const Text(
                      'Forgot your password?',
                      style: TextStyle(
                        color: AppColors.pink_dark,
                        fontSize: 14,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.pink_dark,
                  padding: const EdgeInsets.symmetric(horizontal: 130, vertical: 15),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child:const Text(
                  'Sign in',
                  style: TextStyle(
                    fontSize: 18,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "I don't have an account! ",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500,
                    ),),
                  TextButton(
                    onPressed: () {
                      Get.to(RegisterView());
                    },
                    child:const Text(
                      "Sign up",
                      style: TextStyle(
                        color: AppColors.pink_dark,
                        fontSize: 14,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w600,
                      ),
                    ),),
                ],
              ),
              const SizedBox(height: 20),
              const Text(
                'Or continue with',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ///TODO replace image
                  Image.asset("assets/google.png", height: 50, width: 50),
                  const SizedBox(width: 16),
                  Image.asset("assets/facebook.png", height: 50, width: 50),
                ],
              ),
            ],),
      ),

    );
  }
}
