
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../../../application_style/AppColors.dart';
import '../controllers/OTPVerificationController.dart';

class OTPVerificationScreen extends StatelessWidget {
  final OTPVerificationController controller = Get.put(OTPVerificationController());

   OTPVerificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.pink_light,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Lottie.asset("assets/otp.json"),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                'Enter Verification Code',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 30,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
           const SizedBox(height: 8),
            const Padding(
              padding:  EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                "Please enter the verification code sent to your email.",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(height: 40),
            buildCodeInputFields(context),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.pink_dark,
                padding: const EdgeInsets.symmetric(horizontal: 130, vertical: 15),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              child:const Text(
                'Verify',
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Obx(() => TextButton(
              onPressed: () => controller.resendCode(),
              child: controller.timerActive.value
                  ? Text(
                'Renvoyer le code (${controller.timeRemaining})',
                style:const TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              )
                  :const Text(
                'Renvoyer le code',
                style: TextStyle(
                  color: AppColors.pink_dark,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget buildCodeInputFields(BuildContext context) {
    return Obx(() =>
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
                  6,
                (index) =>
                Container(
                  width: 40,
                  height: 40,
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: controller.controllers[index].text.isNotEmpty
                          ? AppColors.pink_dark
                          : Colors.grey,
                      width: 2,
                    ),
                  ),
                  child: Center(
                    /// TODO change it with CustomInput field
                    child: TextFormField(
                      controller: controller.controllers[index],
                      focusNode: controller.focusNodes[index],
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      maxLength: 1,
                      style: const TextStyle(fontSize: 24),
                      decoration: const InputDecoration(
                        counterText: '',
                        border: InputBorder.none,
                      ),
                      onChanged: (value) {
                        if (value.length == 1 && index < 5) {
                          FocusScope.of(context).requestFocus(
                              controller.focusNodes[index + 1]);
                        }
                        if (value.isEmpty && index > 0) {
                          FocusScope.of(context).requestFocus(
                              controller.focusNodes[index - 1]);
                        }
                      },
                    ),
                  ),
                ),
          ),
        ));
  }
}
