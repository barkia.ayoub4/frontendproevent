import 'package:get/get.dart';
import 'package:pfe/app/modules/register/controllers/register_controller.dart';

import '../../login/controllers/login_controller.dart';
import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.put<LoginController>(LoginController(),permanent: true);
    Get.put<RegisterController>(RegisterController(),permanent: true);

  }
}
