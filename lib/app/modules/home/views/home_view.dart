import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pfe/app/application_style/image_ressources.dart';
import 'package:pfe/app/modules/login/bindings/login_binding.dart';
import 'package:pfe/app/modules/login/views/login_view.dart';
import 'package:pfe/app/modules/register/views/register_view.dart';

import '../../../application_style/AppColors.dart';
import '../../../application_style/style_ressources.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(

        color:AppColors.pink_light,
        child: Stack(
          children: [
            Column(
              children: [
                Image.asset(ImageRessources.EVENT),
                 Text(
                  "Discover Your \n event here ",
                  textAlign: TextAlign.center,
                  style: StyleRessources.largeText
                ),
                const SizedBox(height: 20),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    'Explore all the existing events based on your interest and work major.',
                    textAlign: TextAlign.center,
                    /// TODO replace it with style ressources
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                      height: 1.5,
                    ),
                  ),
                ),
                const SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30,horizontal: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          Get.to(() =>  LoginView(),binding: LoginBinding());
                        },
                        style: ElevatedButton.styleFrom(
                          padding: const EdgeInsets.all(20),
                          fixedSize: const Size(160, 60),
                          textStyle:const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w600,
                          ),
                          backgroundColor:AppColors.pink_dark,
                          foregroundColor: Colors.white,
                          elevation: 10,
                          shadowColor: Colors.grey.withOpacity(0.5),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text('Login'),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: OutlinedButton(
                        onPressed: () {
                          Get.to(() => RegisterView());
                        },
                        style: OutlinedButton.styleFrom(
                          padding: const EdgeInsets.all(20),
                          /// TODO make it responsive
                          fixedSize: const Size(160, 60),
                          textStyle:const TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w600,
                          ),
                          backgroundColor: Colors.white,
                          foregroundColor: Colors.black,
                          elevation: 10,
                          shadowColor: Colors.grey.withOpacity(0.5),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text('Register'),
                      ),
                    ),

                  ],
                ),

                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top:16.0),
                    child: Hero(
                      transitionOnUserGestures: true,
                      tag: 'logo',
                      child: Column(
                        children: [
                          /// replace it with ImageRessources
                          Image.asset("assets/asm_shadow.png", width:155,),
                          const SizedBox(height: 10),
                          const Text(
                            'ASM: All Soft Multimedia',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0xFF494949),
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600,
                              height: 0,
                            ),)
                        ],
                      ),
                    ),
                  ),
                ),


              ],
            ),
          ],
        ),
      ),
    );
  }
}
