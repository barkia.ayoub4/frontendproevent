import 'package:dio/dio.dart';
import 'package:pfe/app/config/ServerConfig.dart';

class ApiService {
  final Dio dio = Dio();

  Future<dynamic> registerUser(Map<String, dynamic> data) async {
    try {
      final response = await dio.post(
        ServerConfig.domainNameServer+ServerConfig.authEndPoints.register,
        data: data,
      );
      return response.data;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }



}
