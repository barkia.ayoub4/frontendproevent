
import 'package:flutter/material.dart';


class AppColors {
  static const t12_primary_color = Color(0xFF026B8A);
  static const beige = Color(0xFFf5edeb);
  static const orange_blue = Color(0xFFEBAC79);
  static const beige_t2 = Color(0xFFAD8E73);
  static const greycard = Color(0xFFE6E0E1);
  static const brown = Color(0xFF834C33);
  static const blue_t2 = Color(0xFF647E99);
  static const t12_primary_color_100 = Color(0xFFE0E0E0);
  static const t12_primary_color_300 = Color(0xFF91D2E2);
  static const t12_primary_color_700 = Color(0xFF2B6C7C);
  static const t12_button_color = Color(0xFF2e97b0);
  static const black_t2 = Color(0xFF5F5F5F);
  static const greyWhite = Color(0xFFBEBEBC);
  static const grey_color = Color(0xFFDADADA);
  static const grey_ = Color(0xFF5F5F5F);
  static const red_oredoo = Color(0xFFD80326);
  static const blue_tt = Color(0xFF28A4D0);
  static const pink_dark = Color.fromARGB(255, 187, 31, 115);
  static const pink_light = Color.fromARGB(255, 253, 244, 244);

  static const t12_error = Color(0xFFFF595E);
  static const t12_success = Color(0xFF36d592);


  static const t12_card_blue_gradient = [
    Color(0xFF91D2E2),
    Color(0xff48B4CE)
  ];
  static const t12_card_golden_yellow_gradient = [
    Color(0xFFFCCB9A),
    Color(0xFFFF9800),
    Color(0xffD68D15),
    Color(0xffB46A11)
  ];
  static const progress_gradient=[
    Color(0xff6599A4),
    Color(0xff4464BB),
    Color(0xff5887A3),

  ];
  static const t12_card_grey_gradient = [
    Color(0xFF929292),
    Color(0xFF5F5F5F)
  ];
  static const t12_card_white_gradient = [
    Colors.white,
    Colors.white
  ];
  static const t12_card_red_gradient = [
    Color(0xFFFF7162),
    Color(0xffF32F4C)
  ];
  static const t12_card_orange_gradient = Color(0xFFFF9449);
  static const t12_card_green_gradient = [
    Color(0xFFA3C94A),
    Color(0xff639419)
  ];
  static const t12_card_purple_gradient = [
    Color(0xFFF1B5E5),
    Color(0xffF1B5E5),
    Color(0xFF4A148C),
  ];
  static const t12_card_light_yellow_gradient = [
    Color(0xFFF8D47D),
    Color(0xffF8D47D)
  ]; // projet existant
  static const grey = Color(0xFFFfaebd7);
  static const t12_card_yellow_gradient = [
    Color(0xFFF9D948),
    Color(0xffF9D948)
  ]; // ouverture prochaine
  static const t12_card_light_white_gradient = [
    Colors.grey,
    Colors.grey
  ];

  static const t12_white = Color(0xFFFFFFFF); //
  static const t12_black = Color(0xff212529); //

  static const t12_text_color_primary = Colors.white; //
  static const t12_disabled_text_color = Colors.grey; //
  static const t12_rich_text_primary = Color(0xFF6C757D); //
  static const blue = Color(0xFF016573); //
  static const t12_rich_text_secondary = Color(0xFFADB5BD); //
  static const t12_form_background_Color =
  Color(0xFFE9ECEF); // Color(0xFFF8F9FA)
  static const t12_form_icon_Color = Color(0xFFADB5BD);

  static const BLUE_GRADIENT = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Color(0xff91D2E2), Color(0xff48B4CE),],
  );

  static const WHITE_GRADIENT = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.white, Color(0x7fffffff)],
  );
  static const orange = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [Colors.brown, Color(0xFFFF9449)],
  );



  static const t12_border_color = Color.fromRGBO(205, 205, 205, 0.2);
  static const cardcolor = Color.fromRGBO(243,234,229,255);

  static const light_grey_color = Color(0xffB6B6B6); // Colors.grey[200];

}