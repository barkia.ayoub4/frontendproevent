

import 'package:flutter/material.dart';

import 'AppColors.dart';

class StyleRessources {
  static  TextStyle largeText = TextStyle(
    color: AppColors.pink_dark,
    fontSize: 40,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    height: 1,
  );
}