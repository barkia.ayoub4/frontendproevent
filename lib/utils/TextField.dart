import 'package:flutter/material.dart';

import '../app/application_style/AppColors.dart';


class CustomInputField extends StatelessWidget {
  const CustomInputField({super.key,required this.label,this.obsecurText, required this.icon,required this.controlller});
  final String label;
  final Icon icon;
  final bool? obsecurText;
  final TextEditingController controlller;


  @override
  Widget build(BuildContext context) {
    return  TextFormField(
      obscureText: obsecurText!,
      autovalidateMode: AutovalidateMode.onUserInteraction,
        validator:(value) {
          if (value == null ||
              value.isEmpty ) {
            return '  Champ is required!';
          }
          return null;
        },
        controller: controlller,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(

          label:Text(label),
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide.none
          ),
          prefixIcon:icon,
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide:const BorderSide(color: AppColors.pink_dark),
          ),
        ),
    );
  }
}

